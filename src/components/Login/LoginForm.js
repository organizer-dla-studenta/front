import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faUser } from '@fortawesome/free-solid-svg-icons'
import { faGoogle, faFacebook } from '@fortawesome/free-brands-svg-icons'
import './LoginForm.css';
import { red } from 'color-name';
import auth from '../../services/authSevice';
import { Link } from 'react-router-dom';

export default class LoginForm extends Component {
	constructor(props){
		super(props);

		this.state = {
			username: "",
			password: "" 
		};

		this.change = this.change.bind(this);
		this.submit = this.submit.bind(this);
	}

	change(data) {
		this.setState({
			[data.target.name]: data.target.value
		});
	}

	submit(data) {
		data.preventDefault();
		auth.Login(this.state.username,this.state.password);
	}
	
    render() {
        return (
			<div className="cont" style={{alignContent: "center"}}>
			<div className="container">
				<div className="d-flex justify-content-center h-100">
					<div className="card">
						<div className="card-header">
							<h3>Zaloguj się</h3>
							<div className="d-flex justify-content-end social_icon">
								<span className="facebookIcon"><FontAwesomeIcon icon={faFacebook} /></span>
								<span className="googleIcon"><FontAwesomeIcon icon={faGoogle} /></span>
							</div>
						</div>
						<div className="card-body">
							<form onSubmit={data => this.submit(data)}>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text"><FontAwesomeIcon icon={faUser} /></span>
									</div>
									<input type="text" className="form-control" name="username" placeholder="login" onChange={data => this.change(data)} value={this.state.username}/>
									
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text"><FontAwesomeIcon icon={faBars} /></span>
									</div>
									<input type="password" className="form-control" name="password" placeholder="hasło" onChange= {data => this.change(data)} value={this.state.password} />
								</div>
								<div className="form-group">
									<input type="submit" value="Zaloguj" className="btn login_btn float-right" style={{backgroundColor:"orange"}} name="submit"/>
								</div>
							</form>
						</div>
						<div className="card-footer">
							<div className="d-flex justify-content-center links" >
								Nie masz konta? <Link className="Nav__link" to="/signUp">Zarejestruj się</Link>
							</div>
							<div className="d-flex justify-content-center">
								<a href="#" style={{color:red}}>Zapomniałeś hasa?</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
        )
    }
}
