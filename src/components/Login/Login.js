import React, { Component } from 'react'
import ParticlesBg from 'particles-bg'
import LoginForm from './LoginForm'

export default class Login extends Component {
    render() {
        return (
            <>
            <LoginForm/>
            <ParticlesBg color="#461D77" num={200} type="cobweb" bg={true} />
            </>
        )
    }
}