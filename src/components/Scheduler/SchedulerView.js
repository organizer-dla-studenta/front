import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  DayView,
  Appointments,
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui';
import ShedulerService from '../../services/shedulerService';
import axios from 'axios'; 
import auth from '../../services/authSevice'; 


const schedulerData = [
  { startDate: '2018-11-01T09:45', endDate: '2018-11-01T11:00', title: 'Meeting' },
  { startDate: '2018-11-01T12:00', endDate: '2018-11-01T13:30', title: 'Go to a gym' },
  { startDate: '2018-11-02T09:45', endDate: '2018-11-02T11:00', title: 'Meeting' },
  { startDate: '2018-11-02T12:00', endDate: '2018-11-02T13:30', title: 'Go to a gym' },
];

export default class SchedulerView extends Component {
	constructor(props){
		super(props);
  }
  schedulerData: []
  state = {
    schedulerData: [],
    urrentDate: '2020-06-23'
  }
  schedulerDataa = [
    { startDate: '2020-06-23T09:45', endDate: '2020-06-23T11:00', title: 'Meeting' }
  ];

  componentDidMount()
  {
    var token = auth.ReturnToken();
    axios.get("http://127.0.0.1:8000/api/v1/event/list", { headers: { Authorization: `Bearer ${token}` } })
        .then(res => 
          {
            this.setState({ schedulerData : res.data.data});
          })
        .catch(err => { 
            console.log(err);
          });


          this.setState({ schedulerData : this.schedulerDataa});
  }
    render() {
      return (
        <>
        <Paper>
          <Scheduler
            data={this.state.schedulerData}
          >
            <ViewState
              currentDate={this.state.currentDate}
            />
            <WeekView
              startDayHour={6}
              endDayHour={20}
            />
            <Appointments />
          </Scheduler>
        </Paper>
        </>
      );
  }
}

