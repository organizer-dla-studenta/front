import React, { Component } from 'react'
import { BrowserRouter as Router, useHistory  } from 'react-router-dom';
import { Button, Nav, Navbar, Form, FormControl } from 'react-bootstrap';
import auth from './../../services/authSevice'; 

class NavbarPage extends Component {
  state = {
    redirect: false
  }
  logoutAndRedirect()
  {
     auth.Logout();
  }

  render() {
    return (
      <>
      <Navbar bg="primary" variant="dark">
        <Navbar.Brand href="#home">ZAI</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="/">Strona główna</Nav.Link>
          <Nav.Link href="/sheduler">Terminarz</Nav.Link>
          <Nav.Link href="/aboutus">O nas</Nav.Link>
        </Nav>
        <Form inline>
          <Button variant="outline-light" onClick={this.logoutAndRedirect} href="/">Wyloguj</Button>
        </Form>
      </Navbar>
    </>
      );
  }
}

export default NavbarPage 