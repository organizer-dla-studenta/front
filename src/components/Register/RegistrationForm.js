import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faUser } from '@fortawesome/free-solid-svg-icons'
import './RegistrationForm.css';
import { red } from 'color-name';
import auth from '../../services/authSevice';

export default class RegistrationForm extends Component {
	constructor(props){
		super(props);

		this.state = {
			username: "",
			password: "",
			passwordConfirmation: ""
		};

		this.change = this.change.bind(this);
		this.submit = this.submit.bind(this);
	}

	change(data) {
		this.setState({
			[data.target.name]: data.target.value
		});
	}

	submit(data) {
		data.preventDefault();

		var bodyFormData = new FormData();
		bodyFormData.append('email', this.username);
		bodyFormData.append('password', this.password);
	
		auth.Register(bodyFormData);
	}
	
    render() {
        return (
			<div className="cont" style={{alignContent: "center"}}>
			<div className="container">
				<div className="d-flex justify-content-center h-100">
					<div className="card">
						<div className="card-header">
							<h3>Rejestracja</h3>
						</div>
						<div className="card-body">
							<form onSubmit={data => this.submit(data)}>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text"><FontAwesomeIcon icon={faUser} /></span>
									</div>
									<input type="text" className="form-control" name="username" placeholder="Adres email" onChange={data => this.change(data)} value={this.state.username}/>
									
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text"><FontAwesomeIcon icon={faBars} /></span>
									</div>
									<input type="password" className="form-control" name="password" placeholder="Hasło" onChange= {data => this.change(data)} value={this.state.password} />
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text"><FontAwesomeIcon icon={faBars} /></span>
									</div>
									<input type="password" className="form-control" name="passwordConfirmation" placeholder="Powtórz hasło" onChange= {data => this.change(data)} value={this.state.passwordConfirmation} />
								</div>
								<div className="form-group">
									<input type="submit" value="Zarejestruj" className="btn login_btn float-right" style={{backgroundColor:"orange"}} name="submit"/>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			</div>
        )
    }
}
