import React, { Component } from 'react';
import './HomePage.css'
import HomeElement from './HomeElement';
import auth from '../../services/authSevice';

function ShowButtons() {
    if (!auth.IsAuthenticated()) {
      return <HomeElement />;
    }
    return null;
  }

export default class HomePage extends Component {
    render() {
        return (
            <>
            <h1>Zadbaj o swój czas</h1>
            <ShowButtons/>
            </>
        )
    }
}