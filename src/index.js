import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter, Route } from 'react-router-dom';
import { ProtectedRoute } from './services/ProtectedRoute';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import NotFound from './components/NotFound/NotFound';
import SignIn from './components/Login/Login';
import SignUp from './components/Register/Register';
import ShedulerView from './components/Scheduler/SchedulerView';
import NavbarPage from './components/Navbar/NavbarPage';
import ShowNavbarPage from './components/Navbar/NavBar';
import HomePage from './components/Home/Home';

function App() {
    return (
        <div>
            <ShowNavbarPage/>
            <Route exact path = "/" component={HomePage}/>
            <ProtectedRoute path = "/sheduler" component={ShedulerView}/>
            <Route path = "/signin" component={SignIn}/>
            <Route path = "/signup" component={SignUp}/>
            <Route path = "notFound" exact={true} component={NotFound}/>
        </div>
    );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, rootElement);
