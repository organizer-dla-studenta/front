import React from 'react';
import Login from './components/Login/Login'
import Scheduler from './components/Scheduler/Scheduler';
import SchedulerView from './components/Scheduler/SchedulerView/SchedulerView';

function App() {
  return <Scheduler/>
}

export default App;
